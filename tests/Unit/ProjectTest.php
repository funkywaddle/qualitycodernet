<?php
namespace Tests\Unit;

use Tests\TestCase;
use App\Services\Projects;
use App\Models\Project;
use Mockery;

class ProjectTest extends TestCase
{
    public function setUp() : void {
        parent::setUp();

        $this->instance(
            Projects::class,
            Mockery::mock(Projects::class)
        );

        $this->instance(
            Project::class,
            Mockery::mock(Project::class)
        );
    }

    public function testGetList()
    {
        $this->assertTrue(true);
    }

    public function testGetSingle()
    {
        $this->assertFalse(false);
    }
}
